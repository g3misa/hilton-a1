document.addEventListener("DOMContentLoaded", function(event) {
    window.onscroll = function() {makeStickyHeader()};

    var header = document.getElementById("header");
    var sticky = header.offsetTop;

    function makeStickyHeader() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }
});